# pull official base image
FROM python:3.8.7-alpine

# set work directory
WORKDIR /usr/src/web

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
RUN pip install --upgrade pip

# copy app's files to dest directory
COPY ./django_blog /usr/src/web

# install libraries
RUN pip install -r requirements.txt

# copy WSGI & ASGI to start the service
RUN cp *sgi.py Blog